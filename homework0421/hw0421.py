#question1#

def q1(x1, y1, z1):
    ans1 = 0
    if x1 >= y1:
        ans1 = x1
    else:
        ans1 = y1
    if z1 >= ans:
        ans1 = z1
    return ans

#question2#

def q2(a):
    ans = 0
    index = 0
    for i in range(len(a)):
        if ans <= a[i]:
            ans = a[i]
            index = i
    return ans, index

#question3#

def q3(a):
    for i in range(1, len(a)):
        a2[i] = 0
    return a

#question4-1#
def q4_1(n):
if n == 0 or n == 1:
    return 1
return q4_1(n-1) + q4_1(n-2)

#question4-2#

def q4_2(n):
    k_2 = 1 #k-2項,初期：1項目#
    k_1 = 1 #k-1項,初期：2項目#
    ans = 0

    if n == 1 or n == 2:
        return 1
    elif:
        for i in range(2, n):
            ans = n_2 + n_1
            n_2 = n_1
            n_1 = ans
        return ans

#question4-3#

'''4-1の場合は同じ数値を何度も求めているため効率が悪い。
4-2の場合は一つの関数の中で完結しているので効率が良い。'''

#question5#

def q5(a):
    if len(a) < 1:
        return a
    pivot = a[0]
    left = []
    right = []
    for i in range(1, len(a)):
        if a[i] <= pivot:
            left.append(a[i])
        else:
            right.append(a[i])
    left = q5(left)
    right = q5(right)
    foo = [pivot]
    return left + foo + right
