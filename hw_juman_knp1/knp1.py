#Exercise68
#coding: utf-8
from pyknp import KNP
import sys
input_sentence = sys.stdin.readline()
knp = KNP()

result = knp.parse(input_sentence)
for bnst in result.bnst_list():
    print("".join(mrph.midasi for mrph in bnst.mrph_list()), end = ' ')
print("\n")

#Exercise69
#模範解答を見ずに解答しました。
from pyknp import KNP
import sys
input_sentence = sys.stdin.readline
knp = KNP()
data = ""
a = []
count = False

for line in iter(input_sentence, ""):
    data += line
    if line.strip() == "EOS":
        result = knp.result(data)
        for bnst in result.bnst_list():
            for mrph in bnst.mrph_list():
                a.append(mrph.midasi)
                if mrph.hinsi == "接頭辞":
                    count = True
            if count == True:
                print("".join(a))
            count = False
            a = []
        data = ""

#Exercise70
#coding: utf-8
from pyknp import KNP
import sys
input_sentence = sys.stdin.readline
knp = KNP()
data = ""
a = []
count = 0

for line in iter(input_sentence, ""):
    data += line
    if line.strip() == "EOS":
        result = knp.result(data)
        for bnst in result.bnst_list():
            for mrph in bnst.mrph_list():
                a.append(mrph.midasi)
                if mrph.hinsi == "名詞":
                    count += 1
            if count >= 2:
                print("".join(a))
            count = 0
            a = []
        data = ""
 
