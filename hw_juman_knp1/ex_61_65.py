#Exercise61
#coding: utf-8
from pyknp import Juman

import sys
input_sentence = sys.stdin.readline()

juman = Juman()
result = juman.analysis(input_sentence)

for mrph in result.mrph_list():
    print("", mrph.midasi, end = ' ')
print("")

#Exercise62
#coding: utf-8
from pyknp import Juman
import sys

juman = Juman()
data = ""
for line in iter(sys.stdin.readline, ""):
    data += line
    if line.strip() == "EOS":
        result = juman.result(data)
        print(",".join(mrph.midasi for mrph in result.mrph_list() if mrph.hinsi == "名詞"))
        data = ""
                    
#Exercise63
#coding: utf-8
from pyknp import Juman
import sys
input_sentence = sys.stdin.readline

juman = Juman()
data = ""
for line in iter(input_sentence, ""):
    data += line
    if line.strip() == "EOS":
        result = juman.result(data)
        print(",".join(mrph.genkei for mrph in result.mrph_list() if mrph.hinsi =="動詞"))
        data = ""

#Exercise64
#coding: utf-8
from pyknp import Juman
import sys
input_sentence = sys.stdin.readline
a = []

juman = Juman()
data = ""
for line in iter(input_sentence, ""):
    data += line
    if line.strip() == "EOS":
        result = juman.result(data)
        for mrph in result.mrph_list():
            a.append(mrph.midasi)
        data = ""

hindo_dict = { }
for i in a:
    if not i in hindo_dict:
        hindo_dict[i] = 1
    else:
        hindo_dict[i] += 1

for key, value in sorted(hindo_dict.items(), key=lambda x:x[1], reverse=True):
    print(key, value)

#Exercise65
#coding: utf-8
from pyknp import Juman
import sys
input_sentence = sys.stdin.readline
word = 0
jyutugo = 0

juman = Juman()
data = ""
for line in iter(input_sentence, ""):
    data += line
    if line.strip() == "EOS":
        result = juman.result(data)
        for mrph in result.mrph_list():
            word += 1
            if mrph.hinsi == "動詞":
                jyutugo += 1
            elif mrph.katuyou1 == "イ形容詞":
                jyutugo += 1
            elif mrph.katuyou1 == "ナ形容詞":
                jyutugo += 1
        data = ""
print("{0:.3f}%".format(100*jyutugo/word))
